<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hcard');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']|R6j#/L=IE(E7$S}4TG*@uNt1l4X!AMMJ:[1O/-j[lGz!/wM6AtX-O.LOs<2%WV');
define('SECURE_AUTH_KEY',  'he?l|~zs+G#l31k}bfI9<AETM.)d.~Mg:]D+T?FY+OY{%/=?b=;`qq}80.C@+Wml');
define('LOGGED_IN_KEY',    'FSj&?s<V-iy7Hg;/Iq3ji,wA[H|5e6-A=z1 CIENjqZXr;lOO[/bM~-yUl-cNmaW');
define('NONCE_KEY',        'r?po9qbuvoFeWV32 +K^GJzq|btjK9HFs8:sy3$fg{<[Gu$t-O!SDuTG,[R>|^m|');
define('AUTH_SALT',        'viwB?8bBD&V{P7JXK.;iT9):`HQ}|>jxaB]IjPKZz4Xb_~)_XaT)-*NI2~m =f o');
define('SECURE_AUTH_SALT', ',uCJ*!5An:-i-^>fnQ&1YY@ZE8niQVG-g.DdX/.>E;U+_R@FffxE1y@V%j lgU;1');
define('LOGGED_IN_SALT',   '7xL{,]P=Ary|T<76Z[p@BLDuhf}&B-,68.|RQt-|V(vAC:IcxOXh-o7OStyIfJf`');
define('NONCE_SALT',       '-gXy98qMtv|5@VL#0d61i6KQ9>13=:OlPNgx&BOnCyn+nfxEmx~1#uE2(S[kGBV)');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
