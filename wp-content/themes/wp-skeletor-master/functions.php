<?php
/**
 * SKEL-ETOR functions and definitions
 *
 * @package WordPress
 * @subpackage SKEL-ETOR
 * @since SKEL-ETOR 1.0
 *
 * match case - find -> replace: 'SKEL-ETOR' -> Template Names ("SKEL-ETOR 2012") E.g. The Nest
 * match case - find -> replace: 'SKEL_ETOR' -> Theme/Front-End names ("SKEL_ETOR_URL") E.g. THE_NEST_CONSTANT
 * match case - find -> replace: 'skel_etor' -> Function/Variable names ("skel_etor"_excerpt();) E.g. the_nest
 * match case - find -> replace: 'skel-etor' -> Option Names (some_function("skel-etor"-option)); E.g. the-nest
 *
 */

require( get_template_directory() . '/theme/constants.php');
require( get_template_directory() . '/theme/debug.php');
require( get_template_directory() . '/theme/classes.php');
require( get_template_directory() . '/theme/functions.php');
require( get_template_directory() . '/theme/enqueue.php');
require( get_template_directory() . '/theme/shortcodes.php');
require( get_template_directory() . '/theme/setup.php');
require( get_template_directory() . '/theme/actions.php');
require( get_template_directory() . '/theme/filters.php');
require( get_template_directory() . '/theme/theme-actions.php');
require( get_template_directory() . '/theme/theme-hooks.php');

add_filter( 'gform_validation_1', 'custom_validation' );
function custom_validation( $validation_result ) {
	// alert("testing");
// 	echo"<pre>";
    $form = $validation_result['form'];
// print_r($form);
// 	echo"</pre>";
    //supposing we don't want input 1 to be a value of 86
//    if ( rgpost( 'input_1' ) == 86

// var_dump(rgpost( 'input_4_1' ));
// if([a-zA-Z0-9- ]*$/.test( rgpost( 'input_1_4_1' )) == false) {
//     // ) {
// if (/^[a-zA-Z0-9- ]*$/.test(rgpost( 'input_1_4_1' )) == false) {
	if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', rgpost( 'input_4_1' )))
{
	// echo "thisss";

        // set the form validation to false
        $validation_result['is_valid'] = false;

        //finding Field with ID of 1 and marking it as failed validation
        foreach( $form['fields'] as &$field ) {
// echo  $field->id;
            //NOTE: replace 1 with the field you would like to validate
            if ( $field->id == '4' ) {
                $field->failed_validation = true;
                $field->validation_message = 'Please remove special chracters( * , # etc. ) from Address fields !';
                break;
            }
        }

    }


    //Assign modified $form object back to the validation result
    $validation_result['form'] = $form;
    return $validation_result;

}
add_filter( 'gform_validation_2', 'custom_validation2' );
function custom_validation2( $validation_result ) {
	// alert("testing");
// 	echo"<pre>";
    $form = $validation_result['form'];

	if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', rgpost( 'input_8_1' )))
{

        $validation_result['is_valid'] = false;

        foreach( $form['fields'] as &$field ) {
            if ( $field->id == '8' ) {
                $field->failed_validation = true;
                $field->validation_message = 'Please remove special chracters( * , # etc. ) from Address fields !';
                break;
            }
        }

    }
	if (empty( rgpost( 'input_6' )))
{

        $validation_result['is_valid'] = false;

        foreach( $form['fields'] as &$field ) {
            if ( $field->id == '68' ) {
                $field->failed_validation = true;
                $field->validation_message = 'Please enter value !';
                break;
            }
        }

    }

    $validation_result['form'] = $form;
    return $validation_result;

}
add_action( 'gform_after_submission_1', 'set_post_content', 10, 2 );
function set_post_content( $entry, $form ) {

	echo "<style>
	#gform_ajax_spinner_".$entry['form_id']."{
		display:none;
	}
#gform_confirmation_message_".$entry['form_id']."{
	background-color:green;
	color:#fff;
	padding:10px;

}
	</style>";
	// print_r($entry);
	?>
	<script  type="text/javascript" >
	jQuery(document).ready(function($) {
		$(".card_content h2").text("<?php echo $entry['1.3'].' '.$entry['1.6']; ?>");
		$(".card_content .mk_email .second_cont").text("<?php echo $entry['2']; ?>");
		$(".card_content .mk_phone .second_cont").text("<?php echo $entry['3']; ?>");
		$(".card_content .mk_address .second_cont").text("<?php echo $entry['4.1'].' '.$entry['4.2']; ?>");
		$(".card_content .mk_address2 .second_cont").text("<?php echo $entry['4.3'].' '.$entry['4.4']; ?>");
		$(".card_content .mk_postcode .second_cont").text("<?php echo $entry['4.5']; ?>");
		$(".card_content .mk_country .second_cont").text("<?php echo  $entry['4.6']; ?>");
	});
	// alert("hi");
	</script>
	<?php
	// echo "<pre>";
	// print_r($entry);
	// echo "</pre>";
// echo "string";
// echo "string";
// exit();
}
// add_action("gform_after_submission_2", "acf_post_submission", 10, 2);

// // add_action("gform_after_submission_YOUR_FORM_ID", "acf_post_submission", 10, 2);

// function acf_post_submission ($entry, $form)
// {
//    $post_id = $entry["post_id"];
//    $values = get_post_custom_values("field_561baeb17bf2d", $post_id);
//    update_field("field_561baeb17bf2d", $values, $post_id);
// }
add_action("gform_after_submission_2", "acf_post_submission", 10, 2);
function acf_post_submission ($entry, $form)
{
	// echo "<pre>";
	// print_r($entry);
	// echo "</pre>";
	// echo "<pre>";
	// print_r($form);
	// echo "</pre>";

		echo "<style>
	#gform_ajax_spinner_".$entry['form_id']."{
		display:none;
	}
#gform_confirmation_message_".$entry['form_id']."{
	background-color:green;
	color:#fff;
	padding:10px;

}
.validation_error{
	display:none;
}
	</style>";
	// print_r($entry);
	$image_mk=explode("|:", $entry['4']);
	$new_image=$image_mk[0];
	$content_images="";
	for ($i=9; $i <16 ; $i++) { 
		$image_mk1=explode("|:", $entry[$i]);
	$new_image1=$image_mk1[0];
	if($new_image1!=null){

	$content_images.='<a href="'.$new_image1.'" data-toggle="lightbox" data-gallery="multiimages" data-title="" class="col-sm-4"><img src="'.$new_image1.'"></a>';
	}
	}

	$url_listing="";
	  $mk13=unserialize($entry[6]);
	  if(is_array($mk13)){
	  foreach ($mk13 as $key => $value) {
	  	$content_url.="<span>".$value."</span><br/>";
	  	# code...
	  }}
	?>
	<script  type="text/javascript" >
	jQuery(document).ready(function($) {
		// alert("hi");
		$("td.ac_title1").text("<?php echo $entry['1']; ?>");
		$("td.ac_content").text("<?php echo $entry['2']; ?>");
		// $('td.ac_img img').attr('src', "<?php echo $new_image; ?>");
		$('td.ac_img').html('<img src="<?php echo $new_image; ?>" />');
		
			$('td.ac_tag').text("<?php echo $entry['3']; ?>");

		$("td.card_content .mk_phone .second_cont").text("<?php echo $entry['3']; ?>");
		$(".ac_add").text("<?php echo $entry['8.1'].' '.$entry['8.2']; ?>");
		$(".ac_add2").text("<?php echo $entry['8.3'].' '.$entry['8.4']; ?>");
		$("td.gallery_mk").html('<?php echo $content_images; ?>');
		$("td.url_mk").html('<?php echo $content_url; ?>');

		// $(".card_content .mk_postcode .second_cont").text("<?php echo $entry['4.5']; ?>");
		// $(".card_content .mk_country .second_cont").text("<?php echo  $entry['4.6']; ?>");
	});

	// alert("hi");
	</script>
	<?php
 $field_key = 'field_561baeb17bf2d';
   $post_id = $entry["post_id"];
   $value = get_field($field_key, $post_id);
   $mk1=unserialize($entry[6]);

  // $mk1=$entry[6];
 
foreach ($mk1 as $key => $value1) {
	# code...
   $value[] = array('url' => $value1);
}
   update_field($field_key, $value, $post_id);

   $gallery13=array();
   for ($i=9; $i <16 ; $i++) { 
   	 $attach_id= explode("|:||:||:||:|", $entry[$i]);
 $attachid=$attach_id[1];
   if( have_rows('mk_gallery') ){
		$gallery = get_field( 'mk_images' );
	} else {
		$gallery = array();
	}
	array_push($gallery13, $attachid);

   }
   // print_r($gallery13);
   foreach ($gallery13 as $key => $value) {
if($value!=null)
{
   		$gallery[] = array(
		'mk_images'			=> $value,
	
	);
   	}
   }

//update_field('field_561c9d78654f1', $attachid,$post_id);//change {field_key} to actual key
update_field('field_561cb74981b04', $gallery,$post_id);//change {field_key} to actual key



    }



//     function my_update_attachment($f,$pid,$t='',$c='') {
//   wp_update_attachment_metadata( $pid, $f );
//   if( !empty( $_FILES[$f]['name'] )) { //New upload
//     require_once( ABSPATH . 'wp-admin/includes/file.php' );
//  require_once( ABSPATH . 'wp-admin/includes/image.php' );
//     // $override['action'] = 'editpost';
//     $override['test_form'] = false;
//     $file = wp_handle_upload( $_FILES[$f], $override );
 
//     if ( isset( $file['error'] )) {
//       return new WP_Error( 'upload_error', $file['error'] );
//     }
 
//     $file_type = wp_check_filetype($_FILES[$f]['name'], array(
//       'jpg|jpeg' => 'image/jpeg',
//       'gif' => 'image/gif',
//       'png' => 'image/png',
//     ));
//     if ($file_type['type']) {
//       $name_parts = pathinfo( $file['file'] );
//       $name = $file['filename'];
//       $type = $file['type'];
//       $title = $t ? $t : $name;
//       $content = $c;
 
//       $attachment = array(
//         'post_title' => $title,
//         'post_type' => 'attachment',
//         'post_content' => $content,
//         'post_parent' => $pid,
//         'post_mime_type' => $type,
//         'guid' => $file['url'],
//       );
 
//       foreach( get_intermediate_image_sizes() as $s ) {
//         $sizes[$s] = array( 'width' => '', 'height' => '', 'crop' => false );
//         $sizes[$s]['width'] = get_option( "{$s}_size_w" ); // For default sizes set in options
//         $sizes[$s]['height'] = get_option( "{$s}_size_h" ); // For default sizes set in options
//    //     $sizes[$s]['crop'] = get_option( "{$s}_crop" ); // For default sizes set in options
//       }
 
//       $sizes = apply_filters( 'intermediate_image_sizes_advanced', $sizes );
 
//       foreach( $sizes as $size => $size_data ) {
//         $resized = image_make_intermediate_size( $file['file'], $size_data['width'], $size_data['height'], $size_data['crop'] );
//         if ( $resized )
//           $metadata['sizes'][$size] = $resized;
//       }
 
//       $attach_id = wp_insert_attachment( $attachment, $file['file'] /*, $pid - for post_thumbnails*/);
 
//       if ( !is_wp_error( $attach_id )) {
//         $attach_meta = wp_generate_attachment_metadata( $attach_id, $file['file'] );
//         wp_update_attachment_metadata( $attach_id, $attach_meta );
//       }
   
//    return array(
//   'pid' =>$pid,
//   'url' =>$file['url'],
//   'file'=>$file,
//   'attach_id'=>$attach_id
//    );
//     }
//   }
// }
// function jdn_create_image_id( $image_url, $parent_post_id = null ) {
	
// 	if( !isset( $image_url ) )
// 		return false;
// 	// Cache info on the wp uploads dir
// 	$wp_upload_dir = wp_upload_dir();
// 	// get the file path
// 	$path = parse_url( $image_url, PHP_URL_PATH );
// 	// File base name
// 	$file_base_name = basename( $image_url );
// 	// Full path
// 	if( site_url() != home_url() ) {
// 		$home_path = dirname( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) );
// 	} else {
// 		$home_path = dirname( dirname( dirname( dirname( __FILE__ ) ) ) );
// 	}
// 	$home_path = untrailingslashit( $home_path );
// 	$uploaded_file_path = $home_path . $path;
// 	// Check the type of file. We'll use this as the 'post_mime_type'.
// 	$filetype = wp_check_filetype( $file_base_name, null );
// 	// error check
// 	if( !empty( $filetype ) && is_array( $filetype ) ) {
// 		// Create attachment title
// 		$post_title = preg_replace( '/\.[^.]+$/', '', $file_base_name );
	
// 		// Prepare an array of post data for the attachment.
// 		$attachment = array(
// 			'guid'           => $wp_upload_dir['url'] . '/' . basename( $uploaded_file_path ), 
// 			'post_mime_type' => $filetype['type'],
// 			'post_title'     => esc_attr( $post_title ),
// 			'post_content'   => '',
// 			'post_status'    => 'inherit'
// 		);
	
// 		// Set the post parent id if there is one
// 		if( !is_null( $parent_post_id ) )
// 			$attachment['post_parent'] = $parent_post_id;
// 		// Insert the attachment.
// 		$attach_id = wp_insert_attachment( $attachment, $uploaded_file_path );
// 		//Error check
// 		if( !is_wp_error( $attach_id ) ) {
// 			//Generate wp attachment meta data
// 			if( file_exists( ABSPATH . 'wp-admin/includes/image.php') && file_exists( ABSPATH . 'wp-admin/includes/media.php') ) {
// 				 require_once( ABSPATH . 'wp-admin/includes/image.php' );
// 			 require_once( ABSPATH . 'wp-admin/includes/media.php' );
// 				$attach_data = wp_generate_attachment_metadata( $attach_id, $uploaded_file_path );
// 				wp_update_attachment_metadata( $attach_id, $attach_data );
// 			} // end if file exists check
// 		} // end if error check
// 		return $attach_id; 
// 	} else {
// 		return false;
// 	} // end if $$filetype
// } // end function get_image_id
// function convert_upload_field_url_to_attachment( $post_id, $field) {

//   $success = false;

// 	$filename = get_post_meta($post_id, $field, true);
		
// 	if(!is_numeric($filename) && $filename != '') {

// 	     $wp_filetype = wp_check_filetype(basename($filename), null );
		
// 	     $attachment = array(
// 	     'post_mime_type' => $wp_filetype['type'],
// 	     'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
// 	     'post_content' => '',
// 	     'post_status' => 'inherit'
// 	    );
	    
// 	    //Get the absolute path of the uploaded image by Gravity Forms
// 	    $abs_path = getcwd();
// 	    $actual_file = str_replace(site_url(),$abs_path,$filename);
// 	    $attach_id = wp_insert_attachment( $attachment, $actual_file, $post_id );
	    
// 	    //Update Media Metadata
// 	    //require_once(ABSPATH . 'wp-admin/includes/image.php');
// 	    //$attach_data = wp_generate_attachment_metadata( $attach_id, $actual_file );
// 	    //wp_update_attachment_metadata( $attach_id, $attach_data );
	    
// 	    if($attach_id) {
// 	    	update_post_meta($post_id, $field, $attach_id);
// 	    	$gallery[]=$attach_id;
// 	    	update_field( 'field_561bae477bf2b', $gallery, $post_id);
// 	    	$success = true;
// 	    }
// 	    echo $attach_id;
	
// 	}

// 	return $success;

// }

// function get_form_uploads( $wp_attachment_data, $entry, $form ) {

// //echo out the array of attachment ids
// var_dump(wp_attachment_data);
// exit();

// }