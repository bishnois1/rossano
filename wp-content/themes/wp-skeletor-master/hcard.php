<?php
/**
 * Template Name: hcard
 *
 */


get_header(); ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		
	});
</script>
<div id="main">
	<div id="content">
<div class="main_hdcard">
<div class="left_hcard">
<?php echo do_shortcode( '[gravityform id="1" title="true" ajax="true" description="true"]' ); ?>

</div>
<div class="right_hcard">
<h1>Hcard Preview</h1>
<div class="card_content">
<h2>..... </h2>
<ul>

<li class="mk_email"><span class="first_cont">Email</span> <span class="second_cont">.....</span>	</li>
<li class="mk_phone"><span class="first_cont">Phone</span> <span class="second_cont">....</span></li>
<li class="mk_address"><span class="first_cont">Address</span> <span class="second_cont">....</span></li>
<li class="mk_address2"><span class="first_cont"></span> <span class="second_cont">....</span></li>
<li class="mk_postcode"><span class="first_cont">Postcode</span> <span class="second_cont">.....</span></li>
<li class="mk_country"><span class="first_cont">Country </span> <span class="second_cont">.....</span></li>
</ul>
</div>

</div>
</div>

	</div>
	<?php get_sidebar(); ?>
</div>


<?php get_footer(); ?>