<?php
 /* Template Name: Acf html Template 3
 */ 
 get_header();
?>
<style type="text/css">
.mypage{
    width: 100%;
    margin: auto;
        padding-bottom: 30px;
}
.mk_left{
     float: left;
    width: 50%;
}
.mk_right{
        float: right;
    width: 50%;
    background-color: #ccc;
}
.mypage h2{
    background-color: #566D7E;
    color:#fff;
    font-size: 30px;
    text-align: center;
    font-weight:600px;
    padding-top: 30px;
    padding-bottom: 30px;
     margin-top: 0;
}
.mypage .mkform{
 /*background: #fff none repeat scroll 0 0;*/
    margin: auto;
    padding: 20px;
    width: 95%;
}
.mypage .myfield{
    border-bottom: 1px solid #566d7e;
    /*padding: 10px 0 25px;*/
    width: 100%;
      clear: both;
      float: left;
}
.mypage .myfield1{
    border-bottom: 1px solid #566d7e;
    padding: 10px 0 12px;
    width: 100%;
      clear: both;
      float: left;
      min-height: 150px;
}
.mypage .myfield2{
    border-bottom: 1px solid #566d7e;
    padding: 10px 0 12px;
    width: 100%;
      clear: both;
      min-height: 150px;
      float: left;
}


.mypage .myfield label, .mypage .myfield1 label{
    color: #566d7e;
    float: left;
    font-size: 18px;
    width: 30%;
 }
.mypage .myfield span{
     float: right;
    width: 55%;
    font-size: 14px;
}
.mypage .myfield1 span{
     float: right;
    width: 55%;
    font-size: 14px;
 }
.mypage .myfield1 span img{
    /*height: 150px;*/
    width: 200px;
}
.clear{
    clear:both;
}
#change {
    background: #000000 none repeat scroll 0 0;
    border: 2px solid #566d7e;
    border-radius: 9px;
    color: #ffffff;
    cursor: pointer;
    font-size: 20px;
    margin-left: auto;
    margin-right: auto;
    margin-top: 10px;
    padding: 10px 20px;
}

            /* i like padding - you can ignore this css. see the actual css / less files in the repository for styling the gallery navigation */
            div.row > div > div.row {
                margin-bottom: 15px;
            }

            body {
                padding-bottom: 50px;
            }

            div.top-header {
                margin-bottom:100px;
            }

            h3.page-header {
                margin-top: 50px;
            }

            figure {
                position: relative;
            }

            figure figcaption {
                font-size: 22px;
                color: #fff;
                text-decoration: none;
                bottom: 10px;
                right: 20px;
                position: absolute;
                background-color: #000;
            }
            code {
                white-space: pre-wrap;       /* css-3 */
                white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
                white-space: -pre-wrap;      /* Opera 4-6 */
                white-space: -o-pre-wrap;    /* Opera 7 */
                word-wrap: break-word;       /* Internet Explorer 5.5+ */
            }
            
            
            /*my css*/

.mkform table {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: -moz-use-text-color #fff #fff;
    border-image: none;
    border-style: none solid solid;
    border-width: medium 1px 1px;
}           
.mkform table th, .mkform table td {
    border-top: 1px solid #ffffff;
    padding: 2% 1%;
    vertical-align: top;
}
.mkform table th {
    background: #566d7e none repeat scroll 0 0;
    color: #ffffff;
    font-family: arial;
    font-size: 20px;
    text-align: left;
    width: 250px;
}
.mkform table td a.col-sm-4 {
    float: left;
    width: 31%;
    margin:1%;
}
.mkform table td a.col-sm-4 img{
    float: left;
    width: 98%;
    height:auto;
}
</style>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url'); ?>/dist/ekko-lightbox.css" rel="stylesheet">

<div class="mypage">

    <div class="mk_left">
         <?php echo do_shortcode( '[gravityform id="2" title="true" ajax="true" description="true"]' ); ?>
    </div>
    <div class="mk_right">
        <h2 class="ac_title"> Hcard Preview</h2>
    <form class="mkform" method="" action="#">
        
    <table cellpadding="2" cellspacing="0" width="100%">
<tr>
<th>Title</th>
<td class="ac_title1">....</td>
</tr>


<tr>
<th>Content</th>
<td class="ac_content">.......</td>
</tr>



<tr>
<th>Image</th>
<td class="ac_img">.....</td>
</tr>



<tr>
<th>Tag</th>
<td class="ac_tag">....</td>
</tr>




<tr>
<th>Address:</th>
<td >
<span class="ac_add">...</span></br>
<span class="ac_add2">.....</span>
</td>
<!-- <td class="ac_add2">h-no-123, chandigarh</td> -->
</tr>




<tr>
<th>ACF Gallery</th>
<td class="gallery_mk">

......
<!-- <a href="http:http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="People walking down stairs" class="col-sm-4">
                                    <img src="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" class="img-responsive">
                                </a>
                                <a href="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Man getting wet" class="col-sm-4">
                                    <img src="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" class="img-responsive">
                                </a>
                                <a href="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Someone lost their dress" class="col-sm-4">
                                    <img src="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" class="img-responsive">
                                </a>
                            </div>
                            <div class="row">
                                <a href="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" data-toggle="lightbox" data-gallery="multiimages" data-footer="Big ass waterfall- using footer instead of title" class="col-sm-4">
                                    <img src="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" class="img-responsive">
                                </a>
                                <a href="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Cool bottle" data-footer="Now fill it with whiskey" class="col-sm-4">
                                    <img src="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" class="img-responsive">
                                </a>
                                <a href="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" data-toggle="lightbox" class="col-sm-4">
                                    <img src="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" class="img-responsive">
                                </a>

  <a href="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Cool bottle" data-footer="Now fill it with whiskey" class="col-sm-4">
                                    <img src="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" class="img-responsive">
                                </a>
                                <a href="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" data-toggle="lightbox" class="col-sm-4">
                                    <img src="http://www.thinkstockphotos.in/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg" class="img-responsive">
                                </a> -->
                                </td>
</tr>


<tr>
<th>ACF Repeater URLs:</th>
<td class="url_mk">
<!-- <span>google.com</span>
<br/>
<span>google.com</span>
<br/> -->
......
</td>
</tr>

<tr>
<!-- <th><button type="button" value="Change value" id="change" name="chan">Change value</button></th> -->
<td></td>


</tr>

</table>
    </form>
    </div>
</div>
 <script src="//code.jquery.com/jquery.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/dist/ekko-lightbox.js"></script>

        <script type="text/javascript">
            $(document).ready(function ($) {

                $("#change").click(function() {
                    $('.ac_title').text("demo");
                    $('.ac_content').text("demo");
                    $('.ac_img img').attr('src', 'http://localhost/wordpress-acf/wp-content/uploads/2015/10/images-1.jpg');;
                    $('.ac_tag').text("demo");
                    $('.ac_add').text("demo");
                    $('.ac_gallery').text("demo");
                    $('.ac_url').text("demo");
                    
                 });



                // delegate calls to data-toggle="lightbox"
                $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
                    event.preventDefault();
                    return $(this).ekkoLightbox({
                        onShown: function() {
                            if (window.console) {
                                return console.log('Checking our the events huh?');
                            }
                        },
                        onNavigate: function(direction, itemIndex) {
                            if (window.console) {
                                return console.log('Navigating '+direction+'. Current item: '+itemIndex);
                            }
                        }
                    });
                });

                //Programatically call
                $('#open-image').click(function (e) {
                    e.preventDefault();
                    $(this).ekkoLightbox();
                });
                $('#open-youtube').click(function (e) {
                    e.preventDefault();
                    $(this).ekkoLightbox();
                });

                // navigateTo
                $(document).delegate('*[data-gallery="navigateTo"]', 'click', function(event) {
                    event.preventDefault();
                    return $(this).ekkoLightbox({
                        onShown: function() {

                            var a = this.modal_content.find('.modal-footer a');
                            if(a.length > 0) {

                                a.click(function(e) {

                                    e.preventDefault();
                                    this.navigateTo(2);

                                }.bind(this));

                            }

                        }
                    });
                });


            });
        </script>


<?php
 get_footer();
 ?>