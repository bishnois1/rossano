<?php
 /* Template Name: Acf html Template 2
 */ 
 get_header();
?>
<style type="text/css">
.mypage{
	width: 100%;
	margin: auto;
		padding-bottom: 30px;
}
.mk_left{
	 float: left;
    width: 50%;
}
.mk_right{
	    float: right;
    width: 50%;
    background-color: #ccc;
}
.mypage h2{
	background-color: #566D7E;
	color:#fff;
	font-size: 30px;
	text-align: center;
	font-weight:600px;
	padding-top: 30px;
	padding-bottom: 30px;
	 margin-top: 0;
}
.mypage .mkform{
 /*background: #fff none repeat scroll 0 0;*/
    margin: auto;
    padding: 20px;
    width: 95%;
}
.mypage .myfield{
	border-bottom: 1px solid #566d7e;
    /*padding: 10px 0 25px;*/
    width: 100%;
      clear: both;
      float: left;
}
.mypage .myfield1{
	border-bottom: 1px solid #566d7e;
    padding: 10px 0 12px;
    width: 100%;
      clear: both;
      float: left;
      min-height: 150px;
}
.mypage .myfield2{
	border-bottom: 1px solid #566d7e;
    padding: 10px 0 12px;
    width: 100%;
      clear: both;
      min-height: 150px;
      float: left;
}


.mypage .myfield label, .mypage .myfield1 label{
	color: #566d7e;
    float: left;
    font-size: 18px;
    width: 30%;
 }
.mypage .myfield span{
	 float: right;
    width: 55%;
    font-size: 14px;
}
.mypage .myfield1 span{
	 float: right;
    width: 55%;
    font-size: 14px;
 }
.mypage .myfield1 span img{
	/*height: 150px;*/
    width: 200px;
}
.clear{
	clear:both;
}
#change{
	 background: #566d7e none repeat scroll 0 0;
    border: 2px solid #566d7e;
    color: #fff;
    font-size: 20px;
    margin-top: 10px;
    padding: 5px 20px;
}

            /* i like padding - you can ignore this css. see the actual css / less files in the repository for styling the gallery navigation */
            div.row > div > div.row {
                margin-bottom: 15px;
            }

            body {
                padding-bottom: 50px;
            }

            div.top-header {
                margin-bottom:100px;
            }

            h3.page-header {
                margin-top: 50px;
            }

            figure {
                position: relative;
            }

            figure figcaption {
                font-size: 22px;
                color: #fff;
                text-decoration: none;
                bottom: 10px;
                right: 20px;
                position: absolute;
                background-color: #000;
            }
			code {
				white-space: pre-wrap;       /* css-3 */
				white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
				white-space: -pre-wrap;      /* Opera 4-6 */
				white-space: -o-pre-wrap;    /* Opera 7 */
				word-wrap: break-word;       /* Internet Explorer 5.5+ */
			}
</style>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url'); ?>/dist/ekko-lightbox.css" rel="stylesheet">

<div class="mypage">

	<div class="mk_left">
     <?php echo do_shortcode( '[gravityform id="2" title="true" ajax="false" description="true"]' ); ?>
	</div>
	<div class="mk_right">
		<h2 class="ac_title"> Title-Testing</h2>
	<!-- <form class="mkform" method="" action="#"> -->
		<div class="mkform">
		<div class="myfield">
			<label>Title</label><span class="ac_title">Testing</span>
		</div>
		<div class="myfield">
			<label>content</label><span class="ac_content">Testing Testing Title-TestingT esting TestingTestingTesting Testing Title-TestingT esting TestingTestingTesting Testing Title-TestingT esting TestingTesting</span>
		</div>
		<div class="myfield1">
			<label>Image</label><span class="ac_img"><img src="http://localhost/wordpress-acf/wp-content/uploads/2015/10/logo.png" alt="imga"/></span>
		</div>
		<div class="myfield">
			<label>Tag</label><span class="ac_tag">Testing Tag name</span>
		</div>
		<div class="myfield">
            <label>Address:</label><span class="ac_add">h-no-123, chandigarh</span>
			<label></label><span class="ac_add2"></span>
		</div>
		<div class="myfield1">
			<label>ACF Gallery:</label><span class="ac_gallery">

			  <div class="row">
                        <div class="col-md-offset col-md">
                            <div class="row">
                                <a href="http://41.media.tumblr.com/f37ac708134914c471073e4c0b47328d/tumblr_mrn3dc10Wa1r1thfzo8_1280.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="People walking down stairs" class="col-sm-4">
                                    <img src="//41.media.tumblr.com/f37ac708134914c471073e4c0b47328d/tumblr_mrn3dc10Wa1r1thfzo8_1280.jpg" class="img-responsive">
                                </a>
                                <a href="http://41.media.tumblr.com/838b44224e39c30cd43490deb11b919d/tumblr_mrn3dc10Wa1r1thfzo1_1280.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Man getting wet" class="col-sm-4">
                                    <img src="//41.media.tumblr.com/838b44224e39c30cd43490deb11b919d/tumblr_mrn3dc10Wa1r1thfzo1_1280.jpg" class="img-responsive">
                                </a>
                                <a href="http://41.media.tumblr.com/e06a3513b0b36843f54bee99aeac689a/tumblr_mrn3dc10Wa1r1thfzo2_1280.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Someone lost their dress" class="col-sm-4">
                                    <img src="//41.media.tumblr.com/e06a3513b0b36843f54bee99aeac689a/tumblr_mrn3dc10Wa1r1thfzo2_1280.jpg" class="img-responsive">
                                </a>
                            </div>
                            <div class="row">
                                <a href="http://41.media.tumblr.com/9d3e6a9c89a856a2ad0be3ab4ca598b2/tumblr_mrn3dc10Wa1r1thfzo4_1280.jpg" data-toggle="lightbox" data-gallery="multiimages" data-footer="Big ass waterfall- using footer instead of title" class="col-sm-4">
                                    <img src="//41.media.tumblr.com/9d3e6a9c89a856a2ad0be3ab4ca598b2/tumblr_mrn3dc10Wa1r1thfzo4_1280.jpg" class="img-responsive">
                                </a>
                                <a href="http://41.media.tumblr.com/9d3e6a9c89a856a2ad0be3ab4ca598b2/tumblr_mrn3dc10Wa1r1thfzo4_1280.jpg" data-toggle="lightbox" data-gallery="multiimages" data-title="Cool bottle" data-footer="Now fill it with whiskey" class="col-sm-4">
                                    <img src="//41.media.tumblr.com/9d3e6a9c89a856a2ad0be3ab4ca598b2/tumblr_mrn3dc10Wa1r1thfzo4_1280.jpg" class="img-responsive">
                                </a>
                                <a href="http://36.media.tumblr.com/de356cd6570d7c26e73979467f296f67/tumblr_mrn3dc10Wa1r1thfzo6_1280.jpg" data-toggle="lightbox" class="col-sm-4">
                                    <img src="//36.media.tumblr.com/de356cd6570d7c26e73979467f296f67/tumblr_mrn3dc10Wa1r1thfzo6_1280.jpg" class="img-responsive">
                                </a>
                            </div>
                        </div>
                    </div>

			 </span>
		</div>
		<div class="myfield">
			<label>ACF Repeater URLs:</label><span class="ac_url">google.com<br/>
			google.com<br/>google.com</span>
		</div>

		<!-- <button type="button" value="Change value" id="change" name="chan">Change value</button> -->
	<!-- </form> -->
    </div>
	</div>
</div>
 <script src="//code.jquery.com/jquery.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/dist/ekko-lightbox.js"></script>

        <script type="text/javascript">
            $(document).ready(function ($) {

            	$("#change").click(function() {
            		$('.ac_title').text("demo");
            		$('.ac_content').text("demo");
            		$('.ac_img img').attr('src', 'http://localhost/wordpress-acf/wp-content/uploads/2015/10/images-1.jpg');;
            		$('.ac_tag').text("demo");
            		$('.ac_add').text("demo");
            		$('.ac_gallery').text("demo");
            		$('.ac_url').text("demo");
            		
           		 });



                // delegate calls to data-toggle="lightbox"
                $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
                    event.preventDefault();
                    return $(this).ekkoLightbox({
                        onShown: function() {
                            if (window.console) {
                                return console.log('Checking our the events huh?');
                            }
                        },
						onNavigate: function(direction, itemIndex) {
                            if (window.console) {
                                return console.log('Navigating '+direction+'. Current item: '+itemIndex);
                            }
						}
                    });
                });

                //Programatically call
                $('#open-image').click(function (e) {
                    e.preventDefault();
                    $(this).ekkoLightbox();
                });
                $('#open-youtube').click(function (e) {
                    e.preventDefault();
                    $(this).ekkoLightbox();
                });

				// navigateTo
                $(document).delegate('*[data-gallery="navigateTo"]', 'click', function(event) {
                    event.preventDefault();
                    return $(this).ekkoLightbox({
                        onShown: function() {

							var a = this.modal_content.find('.modal-footer a');
							if(a.length > 0) {

								a.click(function(e) {

									e.preventDefault();
									this.navigateTo(2);

								}.bind(this));

							}

                        }
                    });
                });


            });
        </script>


<?php
 get_footer();
 ?>